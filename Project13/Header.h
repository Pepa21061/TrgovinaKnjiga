#ifndef HEADER_H
#define HEADER_H

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h> 
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

typedef struct bookInfo {
	char ime[32];
	char autorIme[32];
	char autorPrezime[32];
	char kategorija[32];
	int godinaIzdanja;
	int brojStranica;
	float cijena;
	int ID;
}BOOK;


void printMenu();
void addBook(char* fileName, int bookCounter, BOOK knjiga);
int exitProgram();
int checkCondition(char* str);
void inputBook(BOOK* knjiga, int bookCounter);
BOOK readBook(char* fileName, int id);
int whatIdToRead();
int whatIdToSell();
void deletefile(char* fileName);
void sort(char* filename );
int getCount(char* fileName);
BOOK* readAll(char* fileName);

#endif // !HEADER_H
