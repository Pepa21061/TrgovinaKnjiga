#include "Header.h"

void printMenu() {
	printf("\nPick your options: \n");
	printf("1- Dodavanje novih knjiga u knjige.bin\n");
	printf("2- Citanje iz datoteke knjige.bin .\n");
	printf("3- Sortiranje prema cijeni ASC\n");
	printf("4- Prodaja knjige \n");
	printf("5- Brisanje svih datoteka .bin \n");
	printf("6- Citanje iz datoteke prodano.bin .\n");
	printf("7- Zavrsetak programa.\n");
}

void addBook(char* fileName, int bookCounter, BOOK knjiga) {

	puts(fileName);

	FILE* file = fopen(fileName, "rb+");
	if (file != NULL) {
		fwrite(&bookCounter, sizeof(int), 1, file);
		printf("%d", bookCounter);

		fseek(file, 0, SEEK_END);
		fwrite(&knjiga, sizeof(BOOK), 1, file);

		puts("\n Book added.\n");

		fclose(file);
	}
	else {
		puts("cant open");
	}
}


int getCount(char* fileName) {

	int bookCounter = 0;

	FILE* file = fopen(fileName, "rb+");
	if (file != NULL) {
		fread(&bookCounter, sizeof(int), 1, file);
	}
	else {
		file = fopen(fileName, "wb");
		fwrite(&bookCounter, sizeof(int), 1, file);
	}
	rewind(file);

	return bookCounter;
}


void inputBook(BOOK* knjiga, int bookCounter) {
	printf("Unesite ime knjige: ");
	scanf(" %[^\n]s", knjiga->ime);

	printf("Unesite ime autora: ");
	scanf(" %[^\n]s", knjiga->autorIme);

	printf("Unesite prezime autora: ");
	scanf(" %[^\n]s", knjiga->autorPrezime);

	printf("Unesite kategoriju knjige: ");
	scanf(" %[^\n]s", knjiga->kategorija);

	printf("Unesite godinuIzdanja knjige: ");
	scanf(" %d", &knjiga->godinaIzdanja);

	printf("Unesite broj stranice knjige: ");
	scanf(" %d", &knjiga->brojStranica);

	printf("Unesite cijenu knjige: ");
	scanf("%f", &knjiga->cijena);

	knjiga->ID = bookCounter; 
}

BOOK readBook(char* fileName, int id) {
	FILE* inFile = fopen(fileName, "rb");
	BOOK knjiga;
	int bookCounter, i;
	if (inFile != NULL) {
		fread(&bookCounter, sizeof(int), 1, inFile);
		for (i = 0; i < bookCounter; i++) {
			fread(&knjiga, sizeof(BOOK), 1, inFile);
			if (id == knjiga.ID) {
				printf("\n************************************************");
				printf("\nIme knjige: %s\n", knjiga.ime);
				printf("\nIme i prezime autora: %s %s\n", knjiga.autorIme, knjiga.autorPrezime);
				printf("\nKategorija knjige: %s\n ", knjiga.kategorija);
				printf("\nGodina izdanja knjige: %d\n", knjiga.godinaIzdanja);
				printf("\nBroj stranice: %d\n", knjiga.brojStranica);
				printf("\nCijena Knjige: %f\n", knjiga.cijena);
				printf("\nId knjige: %d\n", knjiga.ID);
				printf("************************************************\n");
				return knjiga;
			}
		}
		fclose(inFile);
	}
	puts("\n---- END OF FILE ----\n");
}

BOOK* readAll(char* fileName) {
	FILE* inFile = fopen(fileName, "rb");
	BOOK allBooks[100];
	int bookCounter, i;
	if (inFile != NULL) {
		fread(&bookCounter, sizeof(int), 1, inFile);
		for (i = 0; i < bookCounter; i++) {
			fread(&allBooks[i], sizeof(BOOK), 1, inFile);
		}
		fclose(inFile);
	}
	puts("\n---- END OF FILE ----\n");
	return allBooks;
}

void sort(char* filename) {
	BOOK sort[100];
	FILE* inFile = fopen(filename, "rb");
	int bookCounter, i;

	if (inFile != NULL) {
		fread(&bookCounter, sizeof(int), 1, inFile);
		for (i = 0; i < bookCounter; i++) {
			fread(&sort[i], sizeof(BOOK), 1, inFile);
		}
			int min = -1;
			int tempCounter = bookCounter;
			for (int i = 0; i < tempCounter -1; i++) {
				min = i;
				for (int j = i + 1; j < tempCounter; j++) {
					if (sort[j].cijena < sort[min].cijena) {
						min = j; 
					}
					if (min != i) {
						BOOK temp;
						temp = sort[min];
						sort[min] = sort[i];
						sort[i] = temp;
					}

				}

			}

			for (int i = 0; i < bookCounter; i++) {
				printf("\n************************************************");
				printf("\nIme knjige: %s\n", sort[i].ime);
				printf("\nIme i prezime autora: %s %s\n", sort[i].autorIme, sort[i].autorPrezime);
				printf("\nKategorija knjige: %s\n ", sort[i].kategorija);
				printf("\nGodina izdanja knjige: %d\n", sort[i].godinaIzdanja);
				printf("\nBroj stranice: %d\n", sort[i].brojStranica);
				printf("\nCijena Knjige: %f\n", sort[i].cijena);
				printf("\nId knjige: %d\n", sort[i].ID);
				printf("************************************************\n");
			}	
			fclose(inFile);
	}
	
}

int whatIdToRead() {

	int id;
	printf("unesite id knjige koju zelite procitat:\n");
	scanf("%d", &id);

	return id;
}

int whatIdToSell() {

	int id;
	printf("unesite id knjige koju zelite kupiti:\n");
	scanf("%d", &id);

	return id;
}

void deletefile(char* fileName) {

	int status = 0;
	status = remove(fileName);

	if (status == 0) {
		printf("Uspjesno obrisana datoteka.\n");
	}
	else {
		printf("Datoteku nije moguce obrisati.\n");
	}

}

int exitProgram() {
	char str[3];
	int i = 0, counter = 0;
	puts("\nJeste li sigurni da zelite zavrsiti program?\n");
	do {
		printf("Unesi da/ne: ");
		fgets(str, 3, stdin);

		if (toupper(str[0]) == 'D' && toupper(str[1]) == 'A' && str[2] == '\0')
			exit(EXIT_SUCCESS);

	} while (checkCondition(str));
	return 1;
}

int checkCondition(char* str) {
	if (toupper(str[0]) == 'N' && toupper(str[1]) == 'E' && toupper(str[2]) == '\0') return 0;
	return 1;
}
