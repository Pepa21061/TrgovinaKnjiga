#include "Header.h"

int main(void) {

	int inputOption;
	char* fileName = "knjige.bin";
	char* fileName2 = "prodano.bin";

	do {

		printMenu();

		scanf("%d", &inputOption);
		int bookCounter;
		int idFromInput;
		BOOK knjiga;
		BOOK* books;

		switch (inputOption) {

		case 1:
			bookCounter = getCount(fileName);
			bookCounter++;

			inputBook(&knjiga, bookCounter);
			
			addBook(fileName, bookCounter, knjiga);
			break;
		case 2:
			bookCounter = getCount(fileName);
			
			books = readAll(fileName);
			for (int i = 0;i < bookCounter;i++)
				readBook(fileName, books[i].ID);
			break;
		
		case 3:
			sort(fileName);
			break;
		case 4:
			//prodaja knjige
			idFromInput = whatIdToSell();
			bookCounter = getCount(fileName2);
			bookCounter++;
			knjiga = readBook(fileName, idFromInput);
			addBook(fileName2, bookCounter, knjiga);
			break;
		case 5:
			deletefile(fileName);
			deletefile(fileName2);
			break;

		case 6:
			//citanje iz Prodano.bin
			bookCounter = getCount(fileName2);

			books = readAll(fileName2);
			for (int i = 0;i < bookCounter;i++)
				readBook(fileName2, books[i].ID);
			break;
		case 7:
			inputOption = exitProgram();
		default:
			puts("\nTa operacija ne postoji!\n");
			break;
		}

	} while (inputOption != 5);

	return 0;
}